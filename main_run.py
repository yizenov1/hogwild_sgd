

def open_output_file(file_path):
    import os
    cur_path = os.path.dirname(__file__)
    full_file_path = cur_path + "/" + file_path
    return full_file_path


def parse_data(input_file, dimension):
    import numpy as np

    data, labels = [], []
    input_data = input_file.readlines()
    for line in input_data:
        line_splits = line.split()
        labels.append(int(line_splits[0]))

        features = np.zeros(dimension)
        features[0] = 1.0  # bias

        for i in range(1, len(line_splits)):
            index_feature = line_splits[i].split(':')
            features[int(index_feature[0])] = index_feature[1]

        data.append(features)
    del input_data
    data = np.asarray(data)
    labels = np.asarray(labels)
    return data, labels


# MAIN THREAD
def run_learning(iterations=100000, learning_rate=.0003, decay=.0003, n_jobs=20):
    from hogwildsgd import HogWildLearning
    import os
    import matplotlib.pyplot as plt
    import sys

    mini_batch_size, dimension = 200, 300+1  # bias
    full_file_path = open_output_file("w8a.txt")
    if os.path.exists(full_file_path):

        if dimension < 1 or iterations < 0 or learning_rate < 0 or decay < 0 or n_jobs < 1:
            print("\n Parameters set incorrectly")
        else:
            opened_file = open(full_file_path, "r")
            sparse_data, y_true = parse_data(opened_file, dimension)
            del opened_file

            hogwild = HogWildLearning(iterations, learning_rate, decay, n_jobs, mini_batch_size, dimension)
            hogwild.fit(sparse_data, y_true)

            # PLOTTING RESULT
            cost_array = hogwild.shared_loss.reshape((hogwild.mini_batch_nbr, 1)).T.tolist()[0]
            time_array = hogwild.shared_times.reshape((hogwild.mini_batch_nbr, 1)).T.tolist()[0]

            for i in range(len(cost_array)):
                print("mini_batch iteration: " + str(i) + " loss: " + str(cost_array[i]) + " time: " + str(time_array[i]))

            moving_average = []
            for i in range(len(cost_array) - hogwild.moving_avg_size):
                temp = sum(cost_array[i:i + hogwild.moving_avg_size])
                temp /= hogwild.moving_avg_size
                moving_average.append(temp)

            plt.figure()
            plt.plot(moving_average)
            plt.ylim(0, max(moving_average))
            plt.title('loss over iterations')
            plt.xlabel("moving avg of last " + str(hogwild.moving_avg_size) + " mini-batches, mini-batch size: " + str(hogwild.chunk_size))
            plt.ylabel('loss')
            plt.show()

            del sys.modules[hogwild.temp_module_name]  # delete shared data

    else:
        print("\nFile not found. put the file in the same directory.")

run_learning()
