
class HogWildLearning:

    def __init__(self, iterations=100, learning_rate=.001, decay=0, n_jobs=-1, chunk_size=32, dimension=1, batch_size=1, moving_avg_size=20):
        from multiprocessing.sharedctypes import Array, Value
        from ctypes import c_double, c_int
        from types import ModuleType
        import sys
        import numpy as np

        self.batch_size = batch_size
        self.iterations = iterations
        self.decay = decay
        self.n_jobs = n_jobs
        self.chunk_size = chunk_size
        self.mini_batch_nbr = int(self.iterations / self.chunk_size)
        self.dimension = dimension
        self.moving_avg_size = moving_avg_size

        self.temp_module_name = '_hogwild_temp_'

        learning_rate = Value(c_double, learning_rate, lock=False)
        self.learning_rate = learning_rate

        weight_update_loss = Value(c_int, 0, lock=False)
        self.weight_update_loss = weight_update_loss

        rate_update_loss = Value(c_int, 0, lock=False)
        self.rate_update_loss = rate_update_loss

        iteration_index = Value(c_int, 0, lock=False)
        self.iteration_index = iteration_index

        # creates module to properly share variables between processes
        weights_shared = Array(c_double, (np.random.normal(size=(self.dimension, 1)) * 1. / np.sqrt(self.dimension)).flat, lock=False)
        #weights_shared = Array(c_double, np.zeros(shape=(self.dimension, 1)).flat, lock=False)
        weights = np.frombuffer(weights_shared)
        self.shared_weights = weights.reshape((len(weights), 1))

        time_shared = Array(c_double, np.zeros(shape=(self.mini_batch_nbr, 1)).flat, lock=False)
        times_measures = np.frombuffer(time_shared)
        self.shared_times = times_measures.reshape((len(times_measures), 1))

        loss_array = Array(c_double, np.zeros(shape=(self.mini_batch_nbr, 1)).flat, lock=False)
        loss = np.frombuffer(loss_array)
        self.shared_loss = loss.reshape((len(loss), 1))

        # making temporary module to store shared weights
        module = ModuleType(self.temp_module_name)
        module.__dict__['weights'] = self.shared_weights
        module.__dict__['loss'] = self.shared_loss
        module.__dict__['learning_rate'] = self.learning_rate
        module.__dict__['weight_loss'] = self.weight_update_loss
        module.__dict__['shared_times'] = self.shared_times
        module.__dict__['rate_loss'] = self.rate_update_loss
        module.__dict__['iteration_index'] = self.iteration_index
        sys.modules[module.__name__] = module

    def fit(self, data, y):
        from sklearn.externals.joblib import Parallel, delayed
        import numpy as np

        np.random.seed(None)  # may change seed value
        y = y.reshape((len(y), 1))

        Parallel(n_jobs=self.n_jobs)(
            delayed(_train_mini_batch)(input_pair=x_and_y, mini_batch_index=idx, batch_size=self.batch_size, decay=self.decay, temp_module_name=self.temp_module_name)
            for idx, x_and_y in enumerate(self.batch_data_generator(data, y))
        )

        print("\nnumber of weight update loses: " + str(self.weight_update_loss.value))
        print("number of learning rate update loses: " + str(self.rate_update_loss.value))

    def batch_data_generator(self, data, y):
        import numpy as np

        indices = np.random.choice(len(data), replace=False, size=len(data))  # randomizing entire dataset
        data, y = data[indices], y[indices]

        reshape_time, idx = data.shape[0] / self.chunk_size, 0
        batch_size = int(self.iterations/float(self.chunk_size))
        for i in range(batch_size):

            if idx >= reshape_time:
                idx = 0
                seed_value = np.random.random_integers(100)
                np.random.seed(seed_value)
                indices = np.random.choice(len(data), replace=False, size=len(data))
                data, y = data[indices], y[indices]

            x_mini_batch = data[idx*self.chunk_size: (idx+1)*self.chunk_size]  # gets chunk by chunk
            y_mini_batch = y[idx*self.chunk_size: (idx+1)*self.chunk_size]
            idx += 1
            yield (x_mini_batch, y_mini_batch)


def _train_mini_batch(input_pair, mini_batch_index, batch_size, decay, temp_module_name):
    import sys
    import numpy as np
    from scipy.special import expit as sigmoid
    import multiprocessing
    import time
    from ctypes import c_double

    data, y = input_pair

    mini_batch_loss_sum = 0
    batch_nbr = int(data.shape[0] / float(batch_size))
    for i in range(batch_nbr):

        sample_x = data[i * batch_size: (i + 1) * batch_size, :]  # single sample
        sample_y = y[i * batch_size: (i + 1) * batch_size]

        nonzero_idx = np.nonzero(sample_x)  # returns zeros and non-zeros in two arrays
        nonzero_idx = nonzero_idx[1]  # non-zeros
        nonzero_x = sample_x[np.arange(len(sample_x)), nonzero_idx]

        selected_weights = sys.modules[temp_module_name].__dict__['weights'][nonzero_idx]  # selects only needed weights

        dot_product = np.dot(nonzero_x, selected_weights)
        dot_product = -sample_y * dot_product
        dot_product = np.exp(dot_product)  # this may go cause overflow

        loss = np.log(1 + dot_product)
        mini_batch_loss_sum += loss

        iteration = sys.modules[temp_module_name].__dict__['iteration_index'].value
        # print("iteration: %i: %.12f %.2f" % (iteration, loss[0][0], time.time()))  # in seconds
        sys.modules[temp_module_name].__dict__['iteration_index'].value += 1

        # Calculate gradient
        sigmoid_value = sigmoid(dot_product)
        gradient = (-sample_y * sigmoid_value)

        learning_rate = sys.modules[temp_module_name].__dict__['learning_rate'].value

        prev_weights = sys.modules[temp_module_name].__dict__['weights']
        process_id = multiprocessing.current_process()

        for idx, index in enumerate(nonzero_idx):
            x_component = sample_x[0, index]
            partial_gradient = learning_rate * gradient * x_component

            old_value = selected_weights[idx]  # we may count number of update loses
            if old_value != sys.modules[temp_module_name].__dict__['weights'][index]:
                sys.modules[temp_module_name].__dict__['weight_loss'].value += 1

            sys.modules[temp_module_name].__dict__['weights'][index] = old_value - partial_gradient  # accessing for single weight is slower ??

        # we may count number of decay update losses or increase decrease speed
        if learning_rate != sys.modules[temp_module_name].__dict__['learning_rate']:
            sys.modules[temp_module_name].__dict__['rate_loss'].value += 1

        sys.modules[temp_module_name].__dict__['learning_rate'].value = learning_rate / float(1 + decay)

    # loss = sys.modules[temp_module_name].__dict__['loss']
    # avg_error = mini_batch_loss_sum[0] / batch_nbr
    # loss[mini_batch_index] = mini_batch_loss_sum[0]
    sys.modules[temp_module_name].__dict__['loss'][mini_batch_index] = mini_batch_loss_sum[0]

    sys.modules[temp_module_name].__dict__['shared_times'][mini_batch_index] = time.time()
